package com.example.webcontroller1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebController1Application {

    public static void main(String[] args) {

        SpringApplication.run(WebController1Application.class, args);

    }

}
