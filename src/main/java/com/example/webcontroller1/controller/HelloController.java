package com.example.webcontroller1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @GetMapping("/hello-world")
    @ResponseBody
    public String showHelloWorld(){

        return "Hello World";

    }

    @GetMapping("/hello-to")
    @ResponseBody
    public String showHelloTo
            (
                    @RequestParam(required = false, defaultValue = "Simplon") String name
            ){

        return "Hello " + name;

    }

}
